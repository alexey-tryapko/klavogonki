import express from 'express';
import dotenv from 'dotenv';
import path from 'path';
import http from 'http';
import socketIO from 'socket.io';
import passport from 'passport';

import routes from './api/routes/index';
import routesWhiteList from './config/routes-white-list.config';
import authorizationMiddleware from './api/middlewares/authorization.middleware';
import errorHandlerMiddleware from './api/middlewares/error-handler.middleware';
import socketHandlers from './socket/handlers';
import { game } from './socket/services/game.service';
import './config/passport.config';

dotenv.config();

const app = express();
const socketServer = http.Server(app);
export const io = socketIO(socketServer);

io.on('connection', socketHandlers.bind(null, game));
app.use(express.json());
app.use(passport.initialize());

app.use('/api/', authorizationMiddleware(routesWhiteList));

routes(app);

const staticPath = path.resolve(`${__dirname}/public`);
app.use(express.static(staticPath));

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.use(errorHandlerMiddleware);

app.listen(process.env.APP_PORT, () => {
  console.log(`Server listening on port ${process.env.APP_PORT}!`);
});

socketServer.listen(3007);