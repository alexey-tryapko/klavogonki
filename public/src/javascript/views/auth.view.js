import View from './view';
import * as authService from '../services/auth.service';
import * as redirect from '../services/redirect.service';

class AuthView extends View {
    constructor(redirectToGame) {
        super();
        this.createAuthForm();
        this.redirectToGame = redirectToGame;
    }

    createAuthForm() {
        this.element = this.createElement({ tagName: 'form' });

        const [emailControl, emailInput] = this.createControl('email', 'email', 'Email');
        const [passwordControl, PasswordInput] = this.createControl('password', 'password', 'Password');
        const loginBtn = this.createElement({
            tagName: 'a',
            classNames: ['btn', 'btn-primary', 'btn-block' ],
        });
        loginBtn.innerText = 'Login';

        loginBtn.addEventListener('click', ev => this.handleLoginClick(ev, emailInput.value, PasswordInput.value));
        this.element.append(emailControl, passwordControl, loginBtn);
    }

    createControl(type, name, placeholder) {
        const controlWrapper = this.createElement({ tagName: 'div', classNames: ['form-group'] });

        const label = this.createElement({ tagName: 'label' });
        label.innerText = placeholder;

        const control = this.createElement({
            tagName: 'input', classNames: ['form-control'], attributes: {
                type,
                name,
                placeholder,
            }
        });

        controlWrapper.append(label, control);
        return [controlWrapper, control];
    }

    async handleLoginClick(ev, login, password) {
        const authPromise = authService.login({email: login, password});
        const res = await authService.handleAuthResponse(authPromise);
        if(res) redirect.game();
    }
};

export default AuthView;