import callWebApi from '../helpers/api.helper';

export const login = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/auth/login',
        type: 'POST',
        request,
    });
    return response.json();
};

const setToken = token => localStorage.setItem('token', token);

const setUser = user => localStorage.setItem('user', JSON.stringify(user));

const setAuthData = (user = null, token = '') => {
    setToken(token);
    setUser(user);
};

export const handleAuthResponse = async authResponsePromise => {
    const { user, token } = await authResponsePromise;
    setAuthData(user, token);
    return token || false;
};


