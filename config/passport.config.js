import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { secret } from './jwt.config';

const users = require('./../data/storage/users.json');

const opts = {
    secretOrKey: secret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

passport.use(new JwtStrategy(opts, async (payload, done) => {
    try {
        const user = users.find(userFromDB => {
            if (userFromDB.email === payload.email) {
                return userFromDB;
            }            
        });
        return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
    } catch (err) {
        return done(err);
    }
}));
passport.use(
    'login',
    new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
        try {
            const user = users.find(userFromDB => {
                if (userFromDB.email === email) {
                    return userFromDB;
                }            
            });
            if (!user) {
                return done({ status: 401, message: 'Incorrect email.' }, false);
            }

            return (password === user.password)
                ? done(null, user)
                : done({ status: 401, message: 'Passwords do not match.' }, null, false);
        } catch (err) {
            return done(err);
        }
    })
);
