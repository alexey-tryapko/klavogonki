const fs = require("fs");
const storagePath = "/../storage/";

const readData = async fileName => {
    const path = __dirname + storagePath + fileName;
    const data = await fs.promises.readFile(path, "utf8").catch(console.error);
    return JSON.parse(data);
};

const writeData = (fileName, data) => {
    const path = __dirname + storagePath + fileName;
    const content = JSON.stringify(data);
    fs.promises.writeFile(path, content).catch(console.error);
};

const storage = fileName => {
    const methods = {
        async getAllData() {
            const storageData = await readData(fileName);
            if (storageData) {
                return storageData;
            } else {
                return false;
            }
        },
        async getDataById(id) {
            const storageData = await readData(fileName);
            if (storageData && id) {
                const index = storageData.findIndex(item => item._id === id + "");
                const res = index !== -1 ? storageData[index] : false;
                return res;
            } else {
                return false;
            }
        },
        async getDataByField(field, value) {
            const storageData = await readData(fileName);
            if (storageData && field && value) {
                const index = storageData.findIndex(item => item[field] === value + "");
                const res = index !== -1 ? storageData[index] : false;
                return res;
            } else {
                return false;
            }
        },
        async addData(data) {
            const storageData = await readData(fileName);
            if (storageData && data) {
                storageData.push(data);
                await writeData(fileName, storageData);
                return true;
            } else {
                return false;
            }
        },
        async updateData(data) {
            const storageData = await readData(fileName);
            if (storageData && data) {
                const index = storageData.findIndex(item => item._id === data._id);
                if (index !== -1) {
                    storageData[index] = data;
                    await writeData(fileName, storageData);
                    return true;
                }
                return false;
            } else {
                return false;
            }
        },
        async deleteData(id) {
            const storageData = await readData(fileName);
            if (id) {
                let index = storageData.findIndex(item => item._id === id + "");
                if (index !== -1) {
                    storageData.splice(index, 1);
                    await writeData(fileName, storageData);
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    };

    return methods;
};

module.exports = storage;
