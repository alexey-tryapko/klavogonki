import authRoutes from './auth.routes';
import gameRoutes from './game.routes';
export default (app) => {
    app.use('/api/auth', authRoutes);
    app.use('/api/game', gameRoutes);
};
