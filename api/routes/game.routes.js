import { Router } from 'express';
import * as gameService from '../services/game.service';

const router = Router();

router
  .get('/text', (req, res, next) => gameService.getText()
    .then(data => res.send(data))
    .catch(next));

export default router;