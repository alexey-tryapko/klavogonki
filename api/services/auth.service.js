import tokenHelper from '../../helpers/token.helper';
const users = require('./../../data/storage/users.json');
``
export const login = async (userFromReq) => ({
    token: tokenHelper.createToken(userFromReq),
    user:  users.find(user => user.email === userFromReq.email),
});

