const texts = require('./../../data/storage/texts.json');
import { textIndex } from '../../socket/services/game.service';
export const getText = async () => await texts[textIndex];
