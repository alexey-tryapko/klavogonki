import jwt from 'jsonwebtoken';
import { secret } from './../../config/jwt.config';
import { io } from './../../server';
import texts from './../../data/storage/texts.json'
import commentService from './commentator.service';
import Game from './../game';

export let textIndex;
let prevTextIndex;

//Using compositon 
const compose = (...fns) => (...args) => fns
    .reduce((args, fn) => [fn(...args)], args);

const currGame = new Game();

export const game = new Proxy(currGame, {
    set(obj, key, value) {
        return setProcessing(obj, key, value);
    }
});

const emitTimeLeftToStart = time => (
    io.to('lobby').emit('timeLeftToGameStart', { time })
);

const emitTimeLeftToEnd = time => {
    io.to('game').emit('timeLeftTOGameEnd', { time })
};

const emitStartGame = players => {
    io.to('game').emit('startGame', { players })
};

const emitEndGame = () => {
    io.to('game').emit('endGame')
};

const emitPlayersData = players => {
    io.to('game').emit('players', { players })
}

const emitComment = comment => {
    io.to('game').emit('comment', { comment })
};

const setPlayers = players => game.setPlayers(players);
const disconectPlayer = email => game.setPlayerNotActive(email);

const commentPreFinish = player => compose(commentService.preFinish, emitComment)(player);
const entereSymbolPlayer = email => {
    const symbols = game.increasePlayerEnteredSymbols(email);
    const leftSymbols = texts[prevTextIndex].text.length - symbols;
    if (leftSymbols === 10) commentPreFinish({email})
}

const commentPlayerFinished = player => compose(commentService.playerFinished, emitComment)(player);
const finishPlayer = email => (game.setPlayerFinished(email), commentPlayerFinished({email}));

const initPlayers = sockets => {
    const players = sockets.map(socket => {
        const { email } = jwt.decode(socket.handshake.query.token);
        socket.on('disconnect', () => disconectPlayer(email));
        socket.on('enterChar', ({ token }) => {
            jwt.verify(token, secret, (error, { email }) => entereSymbolPlayer(email))
        });
        socket.on('finished', ({ token }) => {
            jwt.verify(token, secret, (error, { email }) => finishPlayer(email))
        })
        return { email, usedTime: 0, enteredSymbols: 0, active: true };
    })
    return setPlayers(players);
};

const moveClients = (from = 'game', to = 'lobby') => {
    const fromRoom = io.sockets.adapter.rooms[from];
    const fromRoomClients = fromRoom ? fromRoom.sockets : {};

    const movedSockets = Object.keys(fromRoomClients).map(id => {
        const socket = io.sockets.connected[id];
        socket.leave(from);
        socket.join(to);
        return socket;
    });
    return movedSockets;
};

const commentGreetings = (name = 'Igor') => compose(commentService.greating, emitComment)(name);
const commentInitPlayers = (players = game.players) => compose(commentService.initPlayers, emitComment)(players);
const commentCurrentState = (players = game.players) => {
    if (game.texID) {
        const text = texts[prevTextIndex];
        compose(commentService.currentState, emitComment)(players, text);
    }
}
const commentGameEnded = (players = game.players) => compose(commentService.gameEnded, emitComment)(players);
const commentFarewell = (name = 'Igor') => compose(commentService.farewell, emitComment)(name);

const start = () => compose(moveClients, initPlayers, emitStartGame)('lobby', 'game');
const stop = compose(emitEndGame, moveClients);

function setProcessing(obj, key, value) {
    obj[key] = value;
    switch (key) {
        case 'timeLeftToStart':
            emitTimeLeftToStart(obj[key]);
            return true;
        case 'timeLeftToEnd':
            if ((obj[key] % 20) === 0 && obj[key]) commentCurrentState();
            if (obj[key] === 1) commentGameEnded();
            emitTimeLeftToEnd(obj[key]);
            return true;
        case 'isPlaying':
            obj[key] ?
                (start(), commentGreetings(), commentInitPlayers()) :
                (commentFarewell(), stop());
            return true;
        case 'texID':
            prevTextIndex = textIndex;
            textIndex = obj[key];
            return true;
        case 'players':
            emitPlayersData(obj[key]);
        default:
            return true;
    };
};

